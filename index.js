const express = require('express')
const bodyParser = require('body-parser')
const app = express()
require('dotenv').config()
const port = process.env.PORT

app.use(bodyParser.json())

app.get('/', (req, res) => {
    res.send("hello")
})

app.get('/params/:name?/:id?', (req, res) => {
    res.json({res_params: req.params})
})

app.get('/object', (req, res) => {
    res.json({name:"Hemal"})
})

app.get('/query', (req, res) => {
    res.json({response_id : req.query.id, response_name:req.query.name})
})

app.get('/next',(req, res, next) => {
    console.log(req.body);
    next()
} , (req, res) => {
    res.json("this is next handler")
})

app.post('/userName', (req, res) => {
    res.json(req.body)
})

app.post('/ownerName', (req, res) => {
    // res.json({header : req.headers?.authorization})
    // res.json({header : req.header('authorization')})
    res.json({header : req.abc?.abc})
})

app.all('*', (req, res) => {
    res.send("Route not found")
})

app.listen(port, () => {
    console.log("Listening on", port);
})